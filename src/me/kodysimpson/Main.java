package me.kodysimpson;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        List<String> emilySucks = Arrays.asList("kody", "lena", "emily", "paige", "leilani", "jackson", "donald", "joe", "adolf", "francisco");

        //Filter out names greater than 4 characters and print them out one by one
        emilySucks.stream()
                .filter(name -> {
                    if (name.length() > 4){
                        return true;
                    }else{
                        return false;
                    }
                })
                .forEach(System.out::println);

        ArrayList<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < 10000; i++){
            numbers.add(new Random().nextInt());
        }

        //Get all positive, even numbers
        numbers.stream()
                .filter(number -> {
                    if (number > 0 && (number % 2 == 0)){
                        return true;
                    }
                    return false;
                }).forEach(System.out::println);

        HashSet<Human> people = new HashSet<>();
        people.add(new Human("Bob", 5));
        people.add(new Human("Randy", 12));
        people.add(new Human("Joe", 2));
        people.add(new Human("Elon", 56));

        //Filter Human objects with ages that are smaller than 10
        HashSet<Human> sortedHumans = (HashSet<Human>) people.stream()
                .filter(human -> {
                    if (human.getAge() < 10){
                        return true;
                    }
                    return false;
                }).collect(Collectors.toSet());
        System.out.println(sortedHumans);


    }
}
